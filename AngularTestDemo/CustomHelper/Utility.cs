﻿using AngularTestDemo.Models;
using AngularTestDemo.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace AngularTestDemo.CustomHelper
{
    public class Utility : IUtilityRepository
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        public Utility(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        public void DeleteStaticProfileImage(string imagePath)
        {
            imagePath = imagePath.TrimStart('\\');
            string _imageToBeDeleted = Path.Combine(_hostingEnvironment.WebRootPath, imagePath);
            File.Delete(_imageToBeDeleted);
        }

        public string GenerateToken(ApplicationUser results)
        {
            var key = Encoding.ASCII.GetBytes("YourKey-2374-OFFKDI940NG7:56753253-tyuw-5769-0921-kfirox29zoxv");
            //Generate Token for user 
            var JWToken = new JwtSecurityToken(
                issuer: "http://localhost:50051/",
                audience: "http://localhost:50051/",
                claims: GetUserClaims(results),
                notBefore: new DateTimeOffset(DateTime.Now).DateTime,
                expires: new DateTimeOffset(DateTime.Now.AddHours(2)).DateTime,
                //Using HS256 Algorithm to encrypt Token
                signingCredentials: new SigningCredentials(new SymmetricSecurityKey(key),
                                    SecurityAlgorithms.HmacSha256Signature)
            );
            var token = new JwtSecurityTokenHandler().WriteToken(JWToken);
            return token;
        }

        public IEnumerable<Claim> GetUserClaims(ApplicationUser results)
        {
            IEnumerable<Claim> claims = new Claim[]
            {
                    new Claim(ClaimTypes.Name, results.Firstname + " " + results.Lastname),
                    new Claim("EMAILID", results.Email),
            };
            return claims;
        }

        public string SaveProfilePic(IFormFile file)
        {
            string fileName = "";
            string imagePath = "";
            if (file != null)
            {
                string folderPath = Path.Combine(_hostingEnvironment.WebRootPath, "Images");
                fileName = Guid.NewGuid().ToString() + "_" + file.FileName;
                string filePath = Path.Combine(folderPath, fileName);
                var imageFile = new FileStream(filePath, FileMode.Create);
                file.CopyTo(imageFile);
                imageFile.Dispose();
                imagePath = "\\Images\\" + fileName;
            }
            else
            {
                imagePath = "\\Images\\default\\default.png";
            }
            return imagePath;
        }
    }
}
