﻿using AngularTestDemo.Models;
using AngularTestDemo.ViewModels;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularTestDemo.DataAccessLayer
{
    public interface IUserDataRepository
    {
        Task<IdentityResult> FormRegister(ApplicationUser formData,string password);
        Task<ApplicationUser> UsernameAvailability(string value);
        Task<ApplicationUser> ValidateLogin(LoginVM data);
        Task<IList<string>> GetUserRole(ApplicationUser user);
    }
}
