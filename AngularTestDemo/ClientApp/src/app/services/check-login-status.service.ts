import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AccountService } from './account.service';
import { Router, CanActivate } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class CheckLoginStatusService implements CanActivate{
  
  constructor(private acnt: AccountService,private route: Router) { }

  ngOnInit() {

  }

  canActivate() {
    if(localStorage.getItem('token')===null || localStorage.getItem('token')===undefined){
      this.route.navigate(['']);
    }
    return true;
    
  }
}
