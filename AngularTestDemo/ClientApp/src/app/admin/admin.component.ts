import { Component, OnInit } from '@angular/core';
import { CustomService } from '../services/custom.service';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  constructor(private custom:CustomService,private toastr: ToastrService) { }

  ngOnInit() {
  }

  getUsersDetails(){
    this.custom.getUsersDetails().subscribe(result=>{

    },error=>{
      this.toastr.error('', 'Some Error Occured', {
        timeOut: 1000
      });
    });
  }
}
