import { Component, OnInit } from '@angular/core';
import { EditComponent } from '../edit/edit.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CustomService } from '../services/custom.service';
import { ToastrService } from 'ngx-toastr';

const MODALS = {
  focusFirst: EditComponent
};

@Component({
  selector: 'app-profile-details',
  templateUrl: './profile-details.component.html',
  styleUrls: ['./profile-details.component.css']
})
export class ProfileDetailsComponent implements OnInit {

  imageSrc: string = '';
  fileUpload : File;
  imageRegex=(/\.(jpe?g|png|jpg)$/i);
  userName : string ="";
  email :string ="";
  profilePic : string;
  mobile : string ="";
  name : string ="";
  firstname :string="";
  lastname :string="";
  id : string ="";
  fileError : string;

  constructor(private _modalService: NgbModal,private custom :CustomService,private toastr: ToastrService) {

  }

  ngOnInit() {
    this.getUserDetails();
  }
  getUserDetails(){
    this.custom.getUserDetails(localStorage.getItem('userId')).subscribe(data=>{
        this.userName=data.result.userName;
        this.email = data.result.email;
        this.imageSrc = data.result.photoPath;
        this.mobile = data.result.phoneNumber;
        this.id=data.result.id;
        this.name= data.result.firstname+' '+ data.result.lastname;
        this.firstname=data.result.firstname;
        this.lastname=data.result.lastname;
    },error=>{
      this.toastr.error('', 'Some Error Occured', {
        timeOut: 1000
      });
    })
  }

  editDetails(name: string){
    let userData={
      firstname :this.firstname,
      lastname : this.lastname,
      email :this.email,
      mobile : this.mobile,
      profession :localStorage.getItem('userRole'),
      id :localStorage.getItem('userId')
    }
    const modalRef= this._modalService.open(MODALS[name],{ size: 'lg' });
    modalRef.componentInstance.user = userData;
    modalRef.result.then((result) => {
        console.log(result);
        this.firstname=result.firstname;
        this.lastname=result.lastname;
        this.email=result.email;
        this.mobile=result.mobile;
        this.name=this.firstname+' '+this.lastname;
    });
  }

  importFile(event: { target: { files: File; }; }){
    this.fileUpload = event.target.files[0];   
    console.log('size', this.fileUpload.size);
    if(this.fileUpload.size>500000 || this.imageRegex.test(this.fileUpload.name)===false){
       this.fileError="File should be of Image type and size should be less than 500kb"
    }
    else{
      this.custom.uploadProfileImage(this.email,this.fileUpload).subscribe(result=>{
        var reader = new FileReader();
        reader.onload = this._handleReaderLoaded.bind(this);
        reader.readAsDataURL(this.fileUpload);
        this.fileError="";
        this.toastr.success('', 'Successully Changed', {
          timeOut: 900
        });
      },error=>{
        this.toastr.error('', 'Error in Uploading', {
          timeOut: 1000
        });
      })      
    }
  }

  _handleReaderLoaded(e) {
   var reader = e.target;
   this.imageSrc = reader.result;
   console.log(this.imageSrc);
  }

  changeCssProp(){
    document.getElementById('profPic').style.opacity="0.5";
  }
  
  changeImageOpacity(){
    document.getElementById('profPic').style.opacity="1";
  }
}
