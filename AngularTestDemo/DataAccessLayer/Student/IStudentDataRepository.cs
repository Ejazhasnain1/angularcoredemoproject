﻿using AngularTestDemo.Models;
using AngularTestDemo.ViewModels;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularTestDemo.DataAccessLayer.Student
{
    public interface IStudentDataRepository
    {
        Task<ApplicationUser> FindUserById(string id);
        Task<IdentityResult> EditProfileDetails(ApplicationUser formData);
        Task<ApplicationUser> FindUserByEmail(string email);
        Task<IdentityResult> UploadProfileImage(ApplicationUser formData);
    }
}
