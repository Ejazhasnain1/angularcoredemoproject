﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AngularTestDemo.BusinessLayer;
using AngularTestDemo.CustomHelper;
using AngularTestDemo.ViewModels;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AngularTestDemo.Controllers
{
    [Route("api/[controller]")]
    public class LoginController : Controller
    {
        private readonly IUserAccountRepository _user;
        private readonly IUtilityRepository _utility;
        public LoginController(IUserAccountRepository user, IUtilityRepository utility)
        {
            _user = user;
            _utility = utility;
        }
        [HttpPost("[action]")]
        public async Task<IActionResult> ValidateLogin([FromBody]LoginVM data)
        {
            try
            {
                var result = await _user.ValidateLogin(data);
                if (result != null)
                {
                    return Ok(new
                    {
                        token = _utility.GenerateToken(result),
                        userName = result.UserName,
                        email = result.Email,
                        userId = result.Id,
                        userRole = await _user.GetUserRole(result)
                    });
                }
                return BadRequest(new { message = "Login Error" });
            }catch(Exception)
            {
                return StatusCode(500);
            }
        }
    }
}
