﻿using System.Threading.Tasks;
using AngularTestDemo.Models;
using Microsoft.AspNetCore.Identity;

namespace AngularTestDemo.DataAccessLayer.Student
{
    public class StudentAccountDAL : IStudentDataRepository
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        public StudentAccountDAL(SignInManager<ApplicationUser> signInManager, UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }

        public async Task<IdentityResult> EditProfileDetails(ApplicationUser formData)
        {
            return await _userManager.UpdateAsync(formData);
        }

        public async Task<ApplicationUser> FindUserByEmail(string email)
        {
            return await _userManager.FindByEmailAsync(email);
        }

        public async Task<ApplicationUser> FindUserById(string id)
        {
            return await _userManager.FindByIdAsync(id);
        }

        public async Task<IdentityResult> UploadProfileImage(ApplicationUser formData)
        {
            return await _userManager.UpdateAsync(formData);
        }
    }
}
