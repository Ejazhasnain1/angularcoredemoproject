import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AccountService } from '../services/account.service';
import { CustomService } from '../services/custom.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  @Input() public user;

  fileError : string;
  usernameError : string;
  disableButton : boolean;
  fileUpload : File;
  editDemo:FormGroup;
  nameRegex=/^[a-zA-Z ]{2,30}$/;
  emailRegex=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  numberRegex="(0/91)?[7-9][0-9]{9}"
  imageRegex=(/\.(jpe?g|png|jpg)$/i);
  
  constructor(public modal: NgbActiveModal,private toastr: ToastrService, private accnt:AccountService , private custom:CustomService) { }

  ngOnInit() {
    console.log(this.user)
    this.validateProfileDetails();
  }

  validateProfileDetails(){    
    this.editDemo=new FormGroup({
       id : new FormControl(this.user.id),
       firstname : new FormControl(this.user.firstname, [Validators.required,Validators.pattern(this.nameRegex)]),
       lastname : new FormControl(this.user.lastname,[Validators.pattern(this.nameRegex)]),
       email : new FormControl(this.user.email,[Validators.required,Validators.pattern(this.emailRegex)]),
       mobile : new FormControl(this.user.mobile,[Validators.required,Validators.pattern(this.numberRegex)])
    });
  }

  validateUsername(value: any) {
    this.disableButton=false;
    this.usernameError="Email-Id Available";
    this.accnt.checkUniqueEmail(value).subscribe(result=>{
      this.disableButton=true;
      return this.usernameError=result.message;
    }),error => {
      this.usernameError=error.message;
    }
  }

  submitProfileDetails(){
    this.user.firstname=this.editDemo.get('firstname').value;
    this.user.lastname=this.editDemo.get('lastname').value;
    this.user.email=this.editDemo.get('email').value;
    this.user.mobile=this.editDemo.get('mobile').value;
     this.custom.editProfileDetails(this.editDemo.value).subscribe(result=>{  
      this.modal.close(this.user);    
      this.toastr.success('', 'Details Successfully Saved', {
        timeOut: 1000
      });
     },error=>{
      this.toastr.error('', 'Details not saved', {
        timeOut: 1000
      });
     })
  }
}
