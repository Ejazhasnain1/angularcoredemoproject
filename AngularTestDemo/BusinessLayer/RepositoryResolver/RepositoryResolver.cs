﻿using AngularTestDemo.CustomHelper;
using AngularTestDemo.DataAccessLayer;
using System;

namespace AngularTestDemo.BusinessLayer.RepositoryResolver
{
    public class RepositoryResolver : IRepositoryResolver
    {
        private readonly IServiceProvider _serviceProvider;
        public RepositoryResolver(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }
        public IUserAccountRepository GetRepositoryByName(string name)
        {
            switch (name)
            {
                case "hello": return (IUserAccountRepository)_serviceProvider.GetService(typeof(UserAccountBAL));
                default: return (IUserAccountRepository)_serviceProvider.GetService(typeof(UserAccountDAL));
            }
        }
    }
}
