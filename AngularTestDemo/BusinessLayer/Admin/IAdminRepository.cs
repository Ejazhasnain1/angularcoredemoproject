﻿using AngularTestDemo.Models;
using AngularTestDemo.ViewModels;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularTestDemo.BusinessLayer.Admin
{
    public interface IAdminRepository
    {
        Task<ApplicationUser> FindUserById(string id);
        Task<IdentityResult> EditProfileDetails(EditProfileVM formData);
        Task<ApplicationUser> FindUserByEmail(string email);
        Task<string> UploadProfileImage(EditProfileVM formData);
        Task<List<ApplicationUser>> GetUsersDetails();
    }
}
