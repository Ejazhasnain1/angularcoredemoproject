﻿using AngularTestDemo.CustomHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularTestDemo.BusinessLayer.RepositoryResolver
{
    public interface IRepositoryResolver
    {
        IUserAccountRepository GetRepositoryByName(string name);
    }
}
