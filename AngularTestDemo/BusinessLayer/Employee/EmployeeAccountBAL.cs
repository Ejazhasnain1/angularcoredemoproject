﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AngularTestDemo.CustomHelper;
using AngularTestDemo.DataAccessLayer.Employee;
using AngularTestDemo.Models;
using AngularTestDemo.ViewModels;
using Microsoft.AspNetCore.Identity;

namespace AngularTestDemo.BusinessLayer.Employee
{
    public class EmployeeAccountBAL : IEmployeeRepository
    {
        private readonly IEmployeeDataRepository _employee;
        private readonly IUtilityRepository _utility;
        public EmployeeAccountBAL(IEmployeeDataRepository employee, IUtilityRepository utility)
        {
            _employee = employee;
            _utility = utility;
        }
        public async Task<IdentityResult> EditProfileDetails(EditProfileVM formData)
        {
            var result = await _employee.FindUserById(formData.id);
            if (result != null)
            {
                result.Email = formData.email;
                result.Firstname = formData.firstname;
                result.Lastname = formData.lastname;
                result.PhoneNumber = formData.mobile;
                return await _employee.EditProfileDetails(result);
            }
            return IdentityResult.Failed();
        }

        public async Task<ApplicationUser> FindUserByEmail(string email)
        {
            return await _employee.FindUserByEmail(email);
        }

        public async Task<ApplicationUser> FindUserById(string id)
        {
            return await _employee.FindUserById(id);
        }

        public async Task<string> UploadProfileImage(EditProfileVM formData)
        {
            var result = await _employee.FindUserByEmail(formData.email);
            var tempPath = result.PhotoPath;
            if (result != null)
            {
                string imagePath = _utility.SaveProfilePic(formData.image);
                result.PhotoPath = imagePath;
                var value = await _employee.UploadProfileImage(result);
                if (value.Succeeded)
                {
                    _utility.DeleteStaticProfileImage(tempPath);
                    return imagePath;
                }
            }
            return null;
        }
    }
}
