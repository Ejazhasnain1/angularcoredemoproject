﻿using Microsoft.AspNetCore.Identity;

namespace AngularTestDemo.Models
{
    public class SeedAdminData
    {
        public static void SeedData(UserManager<ApplicationUser> userManager)
        {
            SeedUsers(userManager);
        }

        private static void SeedUsers(UserManager<ApplicationUser> userManager)
        {
            if (userManager.FindByEmailAsync("admin@gmail.com").Result == null)
            {
                var user = new ApplicationUser()
                {
                    Id = "a18be9c0-aa65-4af8-bd17-00bd9344e575",
                    Email = "admin@gmail.com",
                    EmailConfirmed = true,
                    PasswordHash = "Ejaz@12",
                    Firstname = "Admin",
                    UserName = "admin@gmail.com",
                    Profession = "Admin",
                    PhotoPath = "\\Images\\default\\default.png",
                    PhoneNumber = "8877693273",
                    AccessFailedCount = 0,
                    LockoutEnabled = true,
                    PhoneNumberConfirmed = false,
                    TwoFactorEnabled = false
                };
                IdentityResult result = userManager.CreateAsync(user, user.PasswordHash).Result;
                if (result.Succeeded)
                {
                    userManager.AddToRoleAsync(user, "Admin").Wait();
                }
            }
        }
    }
}
