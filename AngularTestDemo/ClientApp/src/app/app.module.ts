import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { CounterComponent } from './counter/counter.component';
import { FetchDataComponent } from './fetch-data/fetch-data.component';
import { MaterialModule } from './material/material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RegistrationFormComponent } from './registration-form/registration-form.component';
import { ToastrModule } from 'ngx-toastr';
import { StudentComponent } from './student/student.component';
import { EmployeeComponent } from './employee/employee.component';
import { AdminComponent } from './admin/admin.component';
import { CheckLoginStatusService } from './services/check-login-status.service';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { EditComponent } from './edit/edit.component';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { ProfileDetailsComponent } from './profile-details/profile-details.component';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    CounterComponent,
    FetchDataComponent,
    RegistrationFormComponent,
    StudentComponent,
    EmployeeComponent,
    AdminComponent,
    EditComponent,
    ProfileDetailsComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule.forRoot(),
    ToastrModule.forRoot(),
    Ng4LoadingSpinnerModule.forRoot(),
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'counter', component: CounterComponent },
      { path: 'fetch-data', component: FetchDataComponent },
      { path: 'register-form', component: RegistrationFormComponent },
      { path: 'student',canActivate:[CheckLoginStatusService], component: StudentComponent },
      { path: 'employee', canActivate:[CheckLoginStatusService],component: EmployeeComponent },
      { path: 'admin', canActivate:[CheckLoginStatusService], component: AdminComponent },

    ]),
    BrowserAnimationsModule,
    MaterialModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [EditComponent],
})
export class AppModule { }
