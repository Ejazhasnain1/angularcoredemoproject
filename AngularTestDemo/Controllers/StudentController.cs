﻿using System;
using System.Threading.Tasks;
using AngularTestDemo.BusinessLayer.Student;
using AngularTestDemo.ViewModels;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AngularTestDemo.Controllers
{
    [Route("api/[controller]")]
    public class StudentController : Controller
    {
        private readonly IStudentRepository _student;
        public StudentController(IStudentRepository student)
        {
            _student = student;
        }
        [HttpGet("[action]")]
        public async Task<IActionResult> GetUserDetails([FromQuery]string values)
        { 
            try
            {
                var details = await _student.FindUserById(values);
                if (details != null)
                {
                    return Ok(new
                    {
                        result = details,
                        message = "Details Fetched"
                    });
                }
                return BadRequest(new { message = "Details not found" });
            }
            catch(Exception)
            {
                return StatusCode(500);
            }            
        }
        [HttpPost("[action]")]
        public async Task<IActionResult> EditProfileDetails([FromBody]EditProfileVM formData)
        {
            try
            {
                var result = await _student.EditProfileDetails(formData);
                if (result.Succeeded)
                {
                    return Ok(new
                    {
                        success = result
                    });
                }
                return BadRequest(new { message = "Details not found" });
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }
        [HttpPost("[action]")]
        public async Task<IActionResult> UploadProfileImage(EditProfileVM input)
        {
            try
            {
                var result = await _student.UploadProfileImage(input);
                if (string.IsNullOrEmpty(result) == false)
                {
                    return Ok(new
                    {
                        success = result
                    });
                }
                return BadRequest(new { message = "Details not found" });
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }
    }
}
