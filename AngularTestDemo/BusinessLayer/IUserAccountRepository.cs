﻿using AngularTestDemo.Models;
using AngularTestDemo.ViewModels;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AngularTestDemo.BusinessLayer
{
    public interface IUserAccountRepository
    {
       Task<ApplicationUser> FormRegister(RegisterVM formData);
       Task<ApplicationUser> UsernameAvailability(string value);
       Task<ApplicationUser> ValidateLogin(LoginVM data);
       Task<IList<string>> GetUserRole(ApplicationUser user);
    }
}
