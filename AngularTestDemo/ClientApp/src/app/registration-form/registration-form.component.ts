import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { passwordMatchValidator } from './passwordMatchValidator';
import { AccountService } from '../services/account.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-registration-form',
  templateUrl: './registration-form.component.html',
  styleUrls: ['./registration-form.component.css'],
  providers: [Ng4LoadingSpinnerService]
})

export class RegistrationFormComponent implements OnInit {
  fileError : string;
  usernameError : string;
  disableButton : boolean;
  fileUpload : File;
  registerDemo:FormGroup;
  nameRegex=/^[a-zA-Z ]{2,30}$/;
  emailRegex=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  passwordRegex=new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{6,})");
  numberRegex="(0/91)?[7-9][0-9]{9}"
  hide=true;
  confirmHide=true;
  imageRegex=(/\.(jpe?g|png|jpg)$/i);
  
  constructor(private spinner: Ng4LoadingSpinnerService,private fb:FormBuilder,private accnt:AccountService,private toastr: ToastrService, private route:Router) {
    
   }

  ngOnInit() {
    this.validateRegistration();
  }
  profession: string[] = [
    'Student', 'Employee'
  ];

  validateRegistration(){    
    this.registerDemo=new FormGroup({
       firstname : new FormControl('', [Validators.required,Validators.pattern(this.nameRegex)]),
       lastname : new FormControl('',[Validators.pattern(this.nameRegex)]),
       email : new FormControl('',[Validators.required,Validators.pattern(this.emailRegex)]),
       profession : new FormControl('',[Validators.required]),
       mobile : new FormControl('',[Validators.required,Validators.pattern(this.numberRegex)]),
       password : new FormControl('',[Validators.required,Validators.pattern(this.passwordRegex)]),
       image : new FormControl(''),
       confirmPassword : new FormControl('')
    }, passwordMatchValidator);
  }

  handleFileInput(event: { target: { files: File; }; }){
    this.fileUpload = event.target.files[0];   
    console.log('size', this.fileUpload.size);
    console.log(this.imageRegex.test(this.fileUpload.name));
    if(this.fileUpload.size>500000 || this.imageRegex.test(this.fileUpload.name)===false){
       this.disableButton=true;
       this.fileError="File should be of Image type and size should be less than 500kb"
    }
    else{
      this.disableButton=false;
      this.fileError="";
    }
  }

  onSubmit() {
    this.spinner.show();
    this.accnt.register(this.registerDemo.value,this.fileUpload).subscribe(result=>{
      console.log(result);
      if(result!==null){
      this.toastr.success('', 'Registration Successful', {
        timeOut: 1000
      });
      this.spinner.hide();
      console.log(result.userRole);
      this.accnt.redirectToComponent(result.userRole);
      }
    },error=>{
      this.toastr.error('', 'Failed', {
        timeOut: 1000
      });
    });
  }

    validateUsername(value: any) {
      this.disableButton=false;
      this.usernameError="Email-Id Available";
      this.accnt.checkUniqueEmail(value).subscribe(result=>{
        this.disableButton=true;
        return this.usernameError=result.message;
      }),error => {
        this.usernameError=error.message;
      }
    }
}
