﻿using AngularTestDemo.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularTestDemo.DataAccessLayer.Employee
{
    public interface IEmployeeDataRepository
    {
        Task<ApplicationUser> FindUserById(string id);
        Task<IdentityResult> EditProfileDetails(ApplicationUser formData);
        Task<ApplicationUser> FindUserByEmail(string email);
        Task<IdentityResult> UploadProfileImage(ApplicationUser formData);
    }
}
