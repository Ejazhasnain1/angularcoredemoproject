import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AccountService } from '../services/account.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [Ng4LoadingSpinnerService]
})
export class HomeComponent implements OnInit {
  
  loginDemo : FormGroup;
  loginError : string = "";
  count : number =5;

  constructor(private spinner: Ng4LoadingSpinnerService,private acnt: AccountService,
             private toaster: ToastrService,private route: Router) {

  }

  ngOnInit() {
    if(localStorage.getItem('token')!==null){
      this.acnt.redirectToComponent(localStorage.getItem('userRole'));
    }
    this.setDisabledValue();
  }

  setDisabledValue() {
      this.loginDemo = new FormGroup({
      email : new FormControl('', [Validators.required]),
      password : new FormControl('', [Validators.required])
    });
  }

  validateLogin(){
      this.spinner.show();
      this.acnt.login(this.loginDemo.value).subscribe(result=>{
        if(result!==null){    
          this.toaster.success('', 'Successfully Logged In', {
            timeOut: 1000
          });
          this.acnt.redirectToComponent(result.userRole[0]);
          this.spinner.hide();
        }
    },error=>{
        if(this.count <= 0){
          alert('You account has been locked. Try after 1 minute');
          setTimeout(() => this.count=5, 60*1000);
        }
        else{
          alert('You have'+(--this.count)+'attempt remaining');
        }
        this.loginError="Invalid Username or Password"
        this.spinner.hide();
    });
  }
}
