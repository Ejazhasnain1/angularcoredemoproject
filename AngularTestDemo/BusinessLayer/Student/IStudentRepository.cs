﻿using AngularTestDemo.Models;
using AngularTestDemo.ViewModels;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularTestDemo.BusinessLayer.Student
{
    public interface IStudentRepository
    {
        Task<ApplicationUser> FindUserById(string id);
        Task<IdentityResult> EditProfileDetails(EditProfileVM formData);
        Task<ApplicationUser> FindUserByEmail(string email);
        Task<string> UploadProfileImage(EditProfileVM formData);
    }
}
