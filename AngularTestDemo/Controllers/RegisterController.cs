﻿using System;
using System.Threading.Tasks;
using AngularTestDemo.BusinessLayer;
using AngularTestDemo.CustomHelper;
using AngularTestDemo.ViewModels;
using Microsoft.AspNetCore.Mvc;


namespace AngularTestDemo.Controllers
{
    [Route("api/Register")]
    public class RegisterController : Controller
    {
        private readonly IUserAccountRepository _user;
        private readonly IUtilityRepository _utility;
        public RegisterController(IUserAccountRepository user, IUtilityRepository utility)
        {
            _user = user;
            _utility = utility;
        }
        [HttpPost("[action]")]
        public async Task<IActionResult> FormRegister([FromForm]RegisterVM formData)
        {
            try
            {
                var result = await _user.FormRegister(formData);
                if (result != null)
                {
                    return Ok(new
                    {
                        token = _utility.GenerateToken(result),
                        userName = result.UserName,
                        email = result.Email,
                        userId = result.Id,
                        userRole = formData.profession
                    });
                }
                return BadRequest(new { message = "Registration not successful" });
            }
            catch(Exception)
            {
                return StatusCode(500);
            }
        }
        [HttpGet("[action]")]
        public async Task<IActionResult> UsernameAvailability([FromQuery]string value)
        {
            try
            {
                var result = await _user.UsernameAvailability(value);
                if (result != null)
                {
                    return Ok(new
                    {
                        userDetails = result,
                        message = "Email Already exists"
                    });
                }
                return BadRequest(new { message = "Email Available" });
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }
    }
}
