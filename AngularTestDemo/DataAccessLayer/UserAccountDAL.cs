﻿using AngularTestDemo.Models;
using AngularTestDemo.ViewModels;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AngularTestDemo.DataAccessLayer
{
    public class UserAccountDAL : IUserDataRepository
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        public UserAccountDAL(SignInManager<ApplicationUser> signInManager, UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }
        public async Task<IdentityResult> FormRegister(ApplicationUser formData,string password)
        {
            var result= await _userManager.CreateAsync(formData, password);
            if (result.Succeeded)
            {
                var value=await _userManager.AddToRoleAsync(formData, formData.Profession);
                return result;
            }
            return result;
        }

        public async Task<IList<string>> GetUserRole(ApplicationUser user)
        {
            return await _userManager.GetRolesAsync(user);
        }

        public async Task<ApplicationUser> UsernameAvailability(string value)
        {
           return await _userManager.FindByEmailAsync(value);
        }

        public async Task<ApplicationUser> ValidateLogin(LoginVM data)
        {
            var user = await _userManager.FindByEmailAsync(data.email);
            if (user != null)
            {
                var result = await _signInManager.CheckPasswordSignInAsync(user, data.password, lockoutOnFailure: true);
                if (result.Succeeded)
                {
                    return user;
                }
                return null;
            }
            return user;
        }
    }
}
