import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class CustomService { 

  constructor(private http:HttpClient) {
    
   }

  getUserDetails(values){
      let val =this.firstCharUpper(localStorage.getItem('userRole'));
      let userdetailsUrl : string = `/api/${val}/GetUserDetails`;
      return this.http.get<any>(userdetailsUrl+"?values="+values+"").pipe(map(result=>{
          return result;
      }));
  }

  editProfileDetails(userDetails){
      console.log(userDetails);
      let val =this.firstCharUpper(localStorage.getItem('userRole'));
      let editProfileUrl : string = `/api/${val}/EditProfileDetails`;
      return this.http.post<any>(editProfileUrl,userDetails).pipe(map(result=>{
        return result;
      },error=>{
         return error;
      }))
  }

  uploadProfileImage(values,fileUpload: File){
    let val =this.firstCharUpper(localStorage.getItem('userRole'));
    let editImageUrl :string =`/api/${val}/UploadProfileImage`;
    console.log(fileUpload);
     let input=new FormData();
     input.append('image',fileUpload);
     input.append('email',values);
     return this.http.post<any>(editImageUrl,input).pipe(map(result=>{
      return result;
     }));
  }

  getUsersDetails(){
    let val = localStorage.getItem('userRole');
    let userDetailsUrl :string =`/api/${val}/GetUsersDetails`;
    return this.http.get<any>(userDetailsUrl).pipe(map(result=>{
      return result;
    }));
  }

  firstCharUpper(value){
    return value.charAt(0).toUpperCase() + value.slice(1);
  }
}
