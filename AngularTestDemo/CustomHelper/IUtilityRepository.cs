﻿using AngularTestDemo.Models;
using AngularTestDemo.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularTestDemo.CustomHelper
{
    public interface IUtilityRepository
    {
        string GenerateToken(ApplicationUser result);
        string SaveProfilePic(IFormFile file);
        void DeleteStaticProfileImage(string imagePath);
    }
}
