import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { AccountService } from '../services/account.service';
import { Router } from '@angular/router';
import { Variable } from '@angular/compiler/src/render3/r3_ast';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent {
  isExpanded = false;
  userName$ : Observable<string>;
  loginStatus$ : Observable<boolean>;

  constructor(private acnt: AccountService,private route: Router) {    
  }

  ngOnInit() {
      this.userName$=this.acnt.getUsername();
      this.loginStatus$ = this.acnt.isUserLoggedIn();
  }

  onLogout(){
    this.acnt.logout();
  }

  redirectUser(){
      this.acnt.redirectToComponent(localStorage.getItem('userRole'));
  }

  collapse() {
    this.isExpanded = false;
  }

  toggle() {
    this.isExpanded = !this.isExpanded;
  }
}
