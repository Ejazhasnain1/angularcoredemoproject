import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable, BehaviorSubject } from 'rxjs';
import * as jwt_decode from "jwt-decode";
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

    constructor(private http:HttpClient,private route: Router) { }

    private baseUrl : string = "/api/Register/FormRegister";
    private uniqueEmailUrl : string = "/api/Register/UsernameAvailability";
    private loginUrl : string ="/api/Login/ValidateLogin";
    private loginStatus=new BehaviorSubject<boolean>(this.checkLoginStatus());
    private userName=new BehaviorSubject<string>(localStorage.getItem('userName'));
    private userRole=new BehaviorSubject<string>(localStorage.getItem('userRole'));


    register(userDetails,fileUpload: File){
      userDetails.image =fileUpload;
      let input=new FormData();
      input.append('image',userDetails.image);
      input.append('firstname',userDetails.firstname);
      input.append('lastname',userDetails.lastname);
      input.append('email',userDetails.email);
      input.append('profession',userDetails.profession);
      input.append('password',userDetails.password);
      input.append('mobile',userDetails.mobile);
      console.log(userDetails.image);
    
    console.log(userDetails); 
    return this.http.post<any>(this.baseUrl,input).pipe(map(result=>{
          if(result!==null){
             this.loginStatus.next(true);
             localStorage.setItem('loginStatus','1');
             localStorage.setItem('token',result.token);
             localStorage.setItem('userName',result.userName);
             localStorage.setItem('email',result.email);
             localStorage.setItem('userId',result.userId);
             localStorage.setItem('userRole',result.userRole);
             this.userName.next(localStorage.getItem('userName'));
             this.userRole.next(localStorage.getItem('userRole'));
          }
          return result;
    },error=>{
        return error;
    }));
  }

  login(value){
       return this.http.post<any>(this.loginUrl,value).pipe(map(result=>{
        console.log(result);
        if(result!==null){
          this.loginStatus.next(true);
          localStorage.setItem('loginStatus','1');
          localStorage.setItem('token',result.token);
          localStorage.setItem('userName',result.userName);
          localStorage.setItem('email',result.email);
          localStorage.setItem('userId',result.userId);
          localStorage.setItem('userRole',result.userRole);
          this.userName.next(localStorage.getItem('userName'));
          this.userRole.next(localStorage.getItem('userRole'));
       }
       return result;
    },error=>{
       console.log("error");
       return error;
    }));
  }

  checkUniqueEmail(value) {
    return this.http.get<any>(this.uniqueEmailUrl+"?value="+value+"").pipe(map(result=>{
      return result;
    }));
  }

   isUserLoggedIn(){
     return this.loginStatus.asObservable();
   }

   getUsername(){
        return this.userName.asObservable();
   }

   getUserRole(){
     return this.userRole.asObservable();
   }

   checkLoginStatus() : boolean
   {
     if(localStorage.getItem('loginStatus') === '1'){
       const token=localStorage.getItem('token');
        if(token === null || token === undefined){
          return false;
        }
        const decoded= jwt_decode(token);
        if(decoded.exp === undefined){
          localStorage.clear();
          return false;
        }
        const date = new Date(0);
        const tokenDate = date.setUTCSeconds(decoded.exp);
        if(tokenDate.valueOf() > new Date().valueOf()){
          return true;
        }
        localStorage.clear();
        return false;
     }
     return false;  
   }

   redirectToComponent(values){
    switch(values){
      case "Employee":  this.route.navigate(['/employee'])
                        break;
      case "Student" :  this.route.navigate(['/student'])
                        break;
      case "Admin" :    this.route.navigate(['/admin']);
                        break;
      default : this.route.navigate(['']);
    }
   }
   
  logout(){
    this.loginStatus.next(false);
    localStorage.setItem('loginStatus','0');
    localStorage.removeItem('token');
    localStorage.removeItem('userName');
    localStorage.removeItem('email');
    localStorage.removeItem('userId');
    localStorage.removeItem('userRole');
    console.log("Successfully Logged out");
    this.route.navigate(['']);
  }   
}
