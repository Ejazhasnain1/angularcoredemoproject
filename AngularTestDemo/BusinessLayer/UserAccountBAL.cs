﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using AngularTestDemo.CustomHelper;
using AngularTestDemo.DataAccessLayer;
using AngularTestDemo.Models;
using AngularTestDemo.ViewModels;
using Microsoft.AspNetCore.Hosting;

namespace AngularTestDemo.BusinessLayer
{
    public class UserAccountBAL : IUserAccountRepository
    {
        private readonly IUserDataRepository _user;
        private readonly IUtilityRepository _utility;
        public UserAccountBAL(IUserDataRepository user, IUtilityRepository utility)
        {
            _user = user;
            _utility = utility;
        }
        public async Task<ApplicationUser> FormRegister(RegisterVM formData)
        {
            string imagePath = _utility.SaveProfilePic(formData.image);  
            var user = new ApplicationUser()
            {
                Email = formData.email,
                PasswordHash=formData.password,
                Firstname = formData.firstname,
                Lastname = formData.lastname,
                UserName = formData.email,
                Profession = formData.profession,
                PhotoPath = imagePath,
                PhoneNumber = formData.mobile
            };
            var result= await _user.FormRegister(user,formData.password);
            return user;
        }

        public async Task<IList<string>> GetUserRole(ApplicationUser user)
        {
            return await _user.GetUserRole(user);
        }

        public async Task<ApplicationUser> UsernameAvailability(string value)
        {
            return await _user.UsernameAvailability(value);
        }

        public async Task<ApplicationUser> ValidateLogin(LoginVM data)
        {
            return await _user.ValidateLogin(data);
        }
    }
}
