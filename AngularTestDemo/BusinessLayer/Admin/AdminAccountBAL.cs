﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AngularTestDemo.CustomHelper;
using AngularTestDemo.DataAccessLayer.Admin;
using AngularTestDemo.Models;
using AngularTestDemo.ViewModels;
using Microsoft.AspNetCore.Identity;

namespace AngularTestDemo.BusinessLayer.Admin
{
    public class AdminAccountBAL : IAdminRepository
    {
        private readonly IAdminDataRepository _admin;
        private readonly IUtilityRepository _utility;
        public AdminAccountBAL(IAdminDataRepository admin, IUtilityRepository utility)
        {
            _admin = admin;
            _utility = utility;
        }
        public async Task<IdentityResult> EditProfileDetails(EditProfileVM formData)
        {
            var result = await _admin.FindUserById(formData.id);
            if (result != null)
            {
                result.Email = formData.email;
                result.Firstname = formData.firstname;
                result.Lastname = formData.lastname;
                result.PhoneNumber = formData.mobile;
                return await _admin.EditProfileDetails(result);
            }
            return IdentityResult.Failed();
        }

        public async Task<ApplicationUser> FindUserByEmail(string email)
        {
            return await _admin.FindUserByEmail(email);
        }

        public async Task<ApplicationUser> FindUserById(string id)
        {
            return await _admin.FindUserById(id);
        }

        public async Task<List<ApplicationUser>> GetUsersDetails()
        {
            return await  _admin.GetUsersDetails();
        }

        public async Task<string> UploadProfileImage(EditProfileVM formData)
        {
            var result = await _admin.FindUserByEmail(formData.email);
            var tempPath = result.PhotoPath;
            if (result != null)
            {
                string imagePath = _utility.SaveProfilePic(formData.image);
                result.PhotoPath = imagePath;
                var value = await _admin.UploadProfileImage(result);
                if (value.Succeeded)
                {
                    _utility.DeleteStaticProfileImage(tempPath);
                    return imagePath;
                }
            }
            return null;
        }
    }
}
